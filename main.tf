terraform {
  required_version = ">= 1.0.0"

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {}

resource "digitalocean_kubernetes_cluster" "ricardom_kubernetes_cluster" {
  name    = "test-focus-k8s-cluster"
  region  = "nyc1"
  version = "1.22.11-do.0"
  node_pool {
    name       = "default-pool"
    size       = "s-1vcpu-2gb"
    auto_scale = false
    node_count = 2
  }
}

resource "digitalocean_kubernetes_node_pool" "app_node_pool" {
  cluster_id = digitalocean_kubernetes_cluster.ricardom_kubernetes_cluster.id
  name       = "test-focus-node-pool"
  size       = "s-2vcpu-4gb"
  node_count = 2
}

resource "digitalocean_container_registry" "container_registry" {
  name = "test-focus-registry"
  region = "nyc3"
  subscription_tier_slug = "starter"
}
