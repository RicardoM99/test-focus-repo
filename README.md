# Resolución de desafío - José Ricardo Majano De Paz

Hasta el momento logré resolver los 6 puntos del desafío.

El stage donde se despliegue la plantilla de terraform solo funciona la primera vez, ya que tuve un problema para que el pipeline guarde el terraform state y no me alcanzó el tiempo para solventarlo.  De momento este stage tiene permitido fallara para que la ejecución del pipeline continue.

El servicio está expuesto utilizando la siguiente url:

```bash
http://165.227.253.220
```

Mantendré el servicio expuesto hasta el día viernes 8 de Julio ya que ese día eliminaré los recursos desplegados en DigitalOcean.

Recomiendo probar el servicio antes de volver a ejecutar el pipeline puesto que el la IP pública del Load Balancer cambiará y ya no se podrá utilizar la IP que indico arriba.

Adjunto el enlace de los apuntes que tomé para solventar este desafío: https://soft-abacus-30b.notion.site/Focus-c07082fa4bbb4b57b107a6a514068e61

Personalmente fue un desafío que me gustó realizar ya que no utilizo en mi día a día ninguna de las herramientas utilizadas para resolverlo (terraform, docker, k8s, helm, DigitalOcean, Gitlab CI) Gracias a la documentación de todas las herramientas utilizadas pude lograr el resultado que se encuentra en este repo.

No es perfecta la solución pero es trabajo honesto 🙂